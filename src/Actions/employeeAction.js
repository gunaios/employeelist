function employeeAction(data) {
    return {
        type: "EMPLOYEELIST_ACTION",
        employeeData: data.user
    }
}


export default function employeeAPI(success, failure) {
    var employeeData = require('./employeeData.json');
    return(dispatch) => {

        if (employeeData.user) {
            success();
            dispatch(employeeAction(employeeData));

        } else {
            failure()
        }
    }
} 