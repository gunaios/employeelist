function loginAction() {
    return {
        type: "LOGIN_ACTION",
        isLogedIn: true
    }
}


export default function loginAPI(data, success, failure) {
    var loginData = require('./loginData.json');
    return(dispatch) => {

        if (data.userName == loginData.username && data.password == loginData.password) {
            success();
            dispatch(loginAction());

        } else {
            failure()
        }
    }
} 