import React, {Component} from 'react';
import {Platform, StyleSheet, TextInput, View, TouchableOpacity, Text} from 'react-native';
import { connect } from 'react-redux';
import loginAPI from '../Actions/loginAction'


class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: ''
        }
    }
  render() {
    return (
      <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder='User Name'
        onChangeText={(userName) => this.setState({userName})}
        value={this.state.userName}
      />
      <TextInput
        style={styles.input}
        placeholder='Password'
        secureTextEntry={true}
        onChangeText={(password) => this.setState({password})}
        value={this.state.password}
      />
      <TouchableOpacity
         style={styles.button}
         onPress={this.onPress}
       >
         <Text> Login </Text>
       </TouchableOpacity>

      </View>
    );
  }

  onPress = () => {
      if (!this.validateEmail(this.state.userName)) {
          alert("Enter Valid User Name")
          return
      }
      const data = {
          userName: this.state.userName,
          password: this.state.password
      }
      this.props.loginAPI(data,
        response => {
            this.props.navigation.navigate('EmployeeListScreen')
        },
        error => {
            alert('Not matched')
        }
      )

  }
  validateEmail = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    return reg.test(text)
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  input: {
      height: 40,
      width: '70%', 
      borderColor: 'gray', 
      borderWidth: 1,
      marginBottom: 30,
      paddingLeft: 5
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10
    }

});

const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => ({
    loginAPI: (data, success, failure) => dispatch(loginAPI(data, success, failure)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);


