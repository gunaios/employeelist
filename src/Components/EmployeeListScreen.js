import React, {Component} from 'react';
import {FlatList, StyleSheet, TextInput, View, TouchableOpacity, Text} from 'react-native';
import { connect } from 'react-redux';
import employeeAPI from '../Actions/employeeAction'


class EmployeeListScreen extends Component {

  render() {
    if (this.props.employeesData.length > 0) {
      return (
        <FlatList
          data= {this.props.employeesData}
          renderItem={({item, index}) => this.renderCell(item, index)}
          ItemSeparatorComponent={() => <View style={{height: 1, backgroundColor: 'lightgray' }} />}
        />
  
      );

    } else {
      return <Text>Users Not Added</Text>
    }

  }

  componentDidMount() {
  this.props.employeeListAPI(
    response => {

    },
    error => {
        alert('Error')
    }
  )
  }

    renderCell = (user) => {
      return <View style = {{flexDirection: 'column', justifyContent: 'space-between', padding: 16}} >
              <Text style = {{fontSize: 20, color: 'black'}}>{user.name}</Text>
              <Text style = {{fontSize: 20, color: 'black'}}>{user.age}</Text>
              <Text style = {{fontSize: 20, color: 'black'}}>{user.gender}</Text>
              <Text style = {{fontSize: 20, color: 'black'}}>{user.email}</Text>
              <Text style = {{fontSize: 20, color: 'black'}}>{user.phoneNo}</Text>
      </View>
  } 
}

const mapStateToProps = (state) => ({
  employeesData: state.EmployeeList
});

const mapDispatchToProps = (dispatch) => ({
    employeeListAPI: ( success, failure) => dispatch(employeeAPI( success, failure)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeListScreen);


