/**
 * @format
 */
import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import LoginScreen from './src/Components/LoginScreen'
import EmployeeListScreen from './src/Components/EmployeeListScreen'
import { Provider } from 'react-redux'
import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import LoginReducer from './src/Reducers/LoginReducer'
import EmployeeList from './src/Reducers/EmployeeListReducer'
import {createStackNavigator, createAppContainer} from 'react-navigation';


const AppReducers = combineReducers({
    LoginReducer,
    EmployeeList
});

const rootReducer = (state, action) => {
	return AppReducers(state,action);
}

let store = createStore(rootReducer, applyMiddleware(thunk));

// export default store;

const MainNavigator = createStackNavigator({
    LoginScreen: {screen: LoginScreen},
    EmployeeListScreen: {screen: EmployeeListScreen,navigationOptions:  {
        title: 'Employees List',
        headerLeft: null,
        gesturesEnabled: false,
}},
  });
  
  const Navigation = createAppContainer(MainNavigator);

class Main extends Component {
    render() {
        return (
            <Provider store = {store}>
                <Navigation />
            </Provider>
        )
    }
}

AppRegistry.registerComponent(appName, () => Main);
